package pcd.lab02.check_act;

public class Counter {

	private int cont;
	private int min, max;
	
	public Counter(int min, int max){
		this.cont = this.min = min;
		this.max = max;
	}
	
	public synchronized void inc() throws OverflowException {
		if (cont + 1 > max){ //check and act : if(cond) action || if(cond) action: ad esempio due thread devono controllare se il file esiste
			//pattern singleton p
			throw new OverflowException();
		}
		cont++;
	}
	//equivalente a dire che questo metodo è wrappato da synchronized(this)
	//il problema è che la classe ora è thread safe, non risparmia dal problema check and act
	public synchronized void dec() throws UnderflowException {
		if (cont - 1 < min){
			throw new UnderflowException();
		}
		cont--;
	}
	
	public synchronized int getValue(){
		return cont;
	}
}
