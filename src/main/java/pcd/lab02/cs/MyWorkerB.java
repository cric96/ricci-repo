package pcd.lab02.cs;

public class MyWorkerB extends Worker {
	
	private Object lock;
	
	public MyWorkerB(String name, Object lock){
		super(name);
		this.lock = lock;
	}

	public void run(){
		while (true){
		  synchronized(lock){ //sezione critica, quando il woker b entra in questa sezione critica nessuno può entrarci
			  action1();	
			  action2(); //dovremo sempre vedere sempre queste due azioni eseguite in sequenza tra le due sezioni critiche
			  //NOTA BENE se qua metto un ciclo infinito vuol dire che tutti gli altri oggetti che proveranno ad accedere alla sezione
			  //critica si bloccheranno indefinitivamenente
		  }
		  action3();
		}
	}
	
	protected void action1(){
		println("b1");
		wasteRandomTime(0,1000);	
	}
	
	protected void action2(){
		println("b2");
		wasteRandomTime(100,200);	
	}
	protected void action3(){
		println("b3");
		wasteRandomTime(1000,2000);	
	}
}
