package pcd.lab02.matmul;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Simple class implementing a matrix.
 * 
 * @author aricci
 *
 */
public class Mat {

	private double[][] mat;

	public Mat(int n, int m) {
		mat = new double[n][m];
	}

	public void initRandom(double factor) {
		java.util.Random rand = new java.util.Random();
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[i].length; j++) {
				mat[i][j] = (int) (rand.nextDouble() * factor);
				// mat[i][j] = rand.nextDouble();
			}
		}
	}

	public void set(int i, int j, double v) {
		mat[i][j] = v;
	}

	public double get(int i, int j) {
		return mat[i][j];
	}

	public int getNRows() {
		return mat.length;
	}

	public int getNColumns() {
		return mat[0].length;
	}

	/**
	 * ---- PRODOTTO MATRICIALE ----
	 *  [X1 X2 X3]    |Y1| |Y2| |Y3|
 	 *  [X4 X5 X6] x  |Y4| |Y5| |Y6|
 	 *  [X7 X8 X9]    |Y7| |Y8| |Y9|
 	 * */
	public static Mat mul(Mat matA, Mat matB) {
		Mat matC = new Mat(matA.getNRows(), matB.getNColumns());
		for (int i = 0; i < matC.getNRows(); i++) {
			computeAllCellInARow(matA,matB,matC,i);
		}
		return matC;
	}

	public static Mat parallelMul(Mat matA, Mat matb, int thread) {
		final int section = matA.getNColumns()/ ((thread - 1) > 0 ? thread - 1 : 1);
		final List<Thread> workers = new ArrayList<>();
		final Mat result = new Mat(matA.getNRows(), matA.getNColumns());
		for(int i = 0; i < thread; i ++) {
			final int currentSection = (section * (i + 1));
			workers.add(new MatrixMultiplicationWorker(matA,
					matb,
					result,i * section,
					currentSection > matA.getNRows() ? matA.getNRows() - i * section: section ));
		}
		workers.forEach(Thread::start);
		workers.forEach(x -> {
			try {
				x.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		return result;
	}

	public void print() {
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[i].length; j++) {
				System.out.print(" " + mat[i][j]);
			}
			System.out.println();
		}
	}
	private static class MatrixMultiplicationWorker extends Thread {
		private final Mat first;
		private final Mat second;
		private final Mat result;
		private final int startRow;
		private final int heightOfSection;

		public MatrixMultiplicationWorker(Mat first,
										  Mat second,
										  Mat result,
										  int startRow,
										  int heightOfSection) {
			this.first = first;
			this.second = second;
			this.result = result;
			this.startRow = startRow;
			this.heightOfSection = heightOfSection;
		}

		@Override
		public void run() {
			for(int i = startRow; i < startRow + heightOfSection; i ++) {
				computeAllCellInARow(first,second,result,i);
			}
		}
	}

	private static void computeAllCellInARow(Mat first, Mat second, Mat result, int i) {
		for(int j = 0; j < first.getNColumns(); j ++) {
			double sum = 0;
			for (int k = 0; k < second.getNColumns(); k++) {
				sum += first.get(i, k) * second.get(k, j);
			}
			result.set(i, j, sum);
		}
	}
}
