package pcd.lab02.lost_updates;

/**
 * couter mutable e quindi non thread safe e può creare problemi
 */
public class UnsafeCounter {

	private int cont;
	
	public UnsafeCounter(int base){
		this.cont = base;
	}
	
	public void inc(){
		cont++;
	}
	
	public int getValue(){
		return cont;
	}
}
