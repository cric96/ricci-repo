package pcd.lab02.lost_updates;

public class TestCounterUnsafe {
	/*
	questi problemi sono pericolosi quando i sistemi devono essere eseguiti "per sempre"
	 */
	public static void main(String[] args) throws Exception {
		int ntimes = 100000; //NB è una fortuna quando la probabilità di andare a in una situazione errata è alta
		/*
		in questo caso stiamo parlando di un concetto di safety : -> dato in mi aspetto che il valore finale sia in * 2 in questo caso
		 maggior è il numero di iterazione maggiore è alta la probabilità di trovare l'error in quanto è più alta la probabilità che
		 i due thread incrementi i contatori
		 */
		UnsafeCounter c = new UnsafeCounter(0);
		Worker w1 = new Worker(c,ntimes);
		Worker w2 = new Worker(c,ntimes);

		Cron cron = new Cron();
		cron.start();
		w1.start();
		w2.start();
		w1.join();
		w2.join();
		cron.stop();
		System.out.println("Counter final value: "+c.getValue()+" in "+cron.getTime()+"ms.");
	}
}
