package pcd.lab01.hello;

public class TestHello2 {

	public static void main(String[] args) throws Exception {
		/* versione con le lamda expression, si descrive il compotortamento del processo passando una lamda expression
		* deve essere passato un Runnable (function interface). Si utilizza solitamente per i task one shot. Buona programmazione vuole
		* che se deve essere un comportamento continuo (ad esempio con un while(true)) è preferibile fare una classe a parte*/
		new Thread(() -> {
			System.out.println("Hello concurrent world! by "+Thread.currentThread().getName());
			try {
				Thread.sleep(3000);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			System.out.println("done.");
		}).start();		
		
		String myName = Thread.currentThread().getName();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
		System.out.println("Thread spawned - I'm "+myName);
		
	}
}
