package pcd.lab01.hello;

public class TestHello {

	public static void main(String[] args) throws Exception {
		/*
		 * con il debugger posso creare scenari poco probabili
		 */
		MyThread myThread = new MyThread("MyWorker");
		myThread.start();		
		
		String myName = Thread.currentThread().getName();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
		/*
			non è detto che con i tempi che definiamo l'ordine delle stampe è sempre lo stesso!
			(si viola il write once e run everywhere) tutto dipende dallo scheduler che decide
			(in modo non deterministico) chi eseguire e come.
		 */
		System.out.println("Thread spawned - I'm "+myName);
		
	}
}
