package pcd.lab01.test03;

/**
 * Test join.
 * 
 * @author aricci
 *
 */
public class Test {
	/*
  	NB! qua attraverso la sincronizzazione siamo sicuri che b3 verrà stampato dopo a2 ! non esisterà mai una situazione nella quale
  	b3 verrà stampato dopo!

  */
	public static void main(String[] args) throws Exception {
		MyWorkerA t = new MyWorkerA("worker-A");
		t.start();
		new MyWorkerB("worker-B", t).start();		
	}

}
