package pcd.lab01.test03;

public class MyWorkerB extends Worker {
	
	private MyWorkerA friend;
	
	public MyWorkerB(String name, MyWorkerA t){
		super(name);
		friend = t;
	}

	public void run(){
		println("b1");
		println("b2");
		try {
			friend.join(); // prima tecnica di sincronizzazione (nb qua già violiamo quello che abbiamo detto sul discorso del chiamare metodo pubblici su oggetti attivi)
		} catch (InterruptedException ex){
			ex.printStackTrace();
		}
		println("b3");
	}
}
