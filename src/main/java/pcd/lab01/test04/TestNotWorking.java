package pcd.lab01.test04;

import java.util.stream.IntStream;

public class TestNotWorking {

	public static void main(String[] args) {
		/*
			questo non va bene! perchè succede il fattaccio, non parte nessuno degli altri core perchè avviene un comportamento di tipo sequenziale
			lo stream computa un valore allora volta quindi creiamo il primo thread e non gli altri. Quando facciamo join mi fermo nel punto e quindi non creo un
			altro thread.
		 */
		int howMany = Runtime.getRuntime().availableProcessors();
		long t0 = System.currentTimeMillis();
		IntStream.rangeClosed(0,howMany-1)
			.mapToObj(i -> {
				try {
					return new Thread(() -> {
						System.out.println("Hello from core "+i);
						double waste = 0;
						for (int j = 0; j < 100000; j++){
							for (int k = 0; k < 1000; k++){
								waste = waste + k*Math.sin(j);
							}
						}
					});
				} catch (Exception ex){
					return null;
			}}).peek(t -> {
				t.start(); 
			}).forEach(t -> {
				try {
					t.join();
				} catch (Exception ex){}
			});
		long t1 = System.currentTimeMillis();
		System.out.println("Time elapsed: "+ (t1-t0));
		
	}

}
