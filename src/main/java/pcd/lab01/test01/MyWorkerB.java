package pcd.lab01.test01;
/*
	Comportamento attivo.
	achive task -> ho un obiettivo da raggiungere -> computo fino a quando non raggiungo l'obiettivo e poi mi fermo
	maintance task -> non hanno un obiettivo da raggiungere ma deve garantire che il sistema abbiamo una determinata caratteristica
	NB! usando il paradigma ad agenti riesco a descriverlo in modo diretto (è semplicemente un task) con OO invece devo comunque
	supporre che esista un processo esterno con un flusso di controllo.
 */
public class MyWorkerB extends Worker {
	
	public MyWorkerB(String name){
		super(name);
	}

	public void run(){
		while (true){ //stride, esisterà sempre una condizione per la quale il sistema si ferma.
		  action1();	
		  action2();
		}
	}
	
	protected void action1(){
		//queste istruzioni non fanno attesa ma occupano per più tempo la cpu
		/*
		String s = "";
		for (int i = 0; i < 100000; i++){
			for (int j = 0; j <20000; j++){
				s = s+(((char)i) + ((char)j));
			}
		}*/
		println("b1");
		sleepForRandomTime(0,10); //si simula il fatto che un'azione avvenga in modo non deterministico
	}
	
	protected void action2(){
		/*
		String s = "";
		for (int i = 0; i < 100000; i++){
			for (int j = 0; j <20000; j++){
				s = s+(((char)i) + ((char)j));
			}
		}*/
		println("b2");
		sleepForRandomTime(100,200);
	}
}
