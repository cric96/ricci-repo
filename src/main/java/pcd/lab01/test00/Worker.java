package pcd.lab01.test00;

/**
 * Base class for very simple agent structure
 * 
 * @author aricci
 *
 */
public abstract class Worker extends Thread {
	/*
	 * sono un agente che fa qualcosa ma non ha metodi pubblici.
	 * incapsula il flusso di controllo ed evito che si altera
	 * da fuori attraverso le chiamate pubbliche. Un worker non chiama mai
	 * un metodo su un altro worker. L'unico modo per comunicare è quello di
	 * usare oggetti passivi (ad esempio usando il monitor).
	 * Con questa disciplina sappiamo che un'entità attiva ha senso che abbia dei metodi
	 * ma sono delle azioni che lo stesso agente può chiamare (e non all'esterno)
	 * noi "capiamo" l'agente come un insieme di azioni, i metodi sono sempre utili per decomporre
	 * la logica del worker. i metodi sono protetti ma non privati perchè ancora può esistere il
	 * concetto di ereditarietà nei concetti attivi. Cosa vuol dire che "estendo" il comportamento
	 * di un'entità attiva?
*/
	public Worker(String name){
		super(name);
	}

	protected void sleepFor(long ms){
		/*
		interrupt è utile quando c'è un thread bloccato su una wait su un oggetto condiviso.
		su synchronized invece se è bloccato là non si può più sbloccare la situazione
		 */
		try {
			sleep(ms);
		} catch (InterruptedException ex){
			ex.printStackTrace();
		}
	}

	protected void print(String msg){
		synchronized (System.out){
			System.out.print(msg);
		}
	}

	protected void println(String msg){
		synchronized (System.out){
			System.out.println(msg);
		}
	}
}
