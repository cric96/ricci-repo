package pcd.lab03.lost_updates.sol;

import java.util.concurrent.Semaphore;

public class Worker extends Thread{
	
	private UnsafeCounter counter;
	private int ntimes;
	private Semaphore mutex;
	
	public Worker(UnsafeCounter c, int ntimes, Semaphore mutex){
		counter = c;
		this.ntimes = ntimes;
		this.mutex = mutex;
	}
	
	public void run(){
		for (int i = 0; i < ntimes; i++){
			//synchronized da un certo punto di vista è meglio di questa soluzione per via del fatto che ci dobbiamo ricordare di rilasciare sempre il mutex
			try {
				mutex.acquire();
				counter.inc();
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				mutex.release();
			}
		}
	}
}
