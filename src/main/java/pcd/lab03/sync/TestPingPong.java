package pcd.lab03.sync;

import java.util.concurrent.Semaphore;

/**
 * RICORDA! La sincronizzazione serve per ordinare le azioni!
 */
public class TestPingPong {

	public static void main(String[] args) {
		
		Counter counter = new Counter(0);
		
		Semaphore isPingerTurn = new Semaphore(0);
		Semaphore isPongerTurn = new Semaphore(0);
		
		new Pinger(counter, isPingerTurn, isPongerTurn).start();
		new Ponger(counter, isPongerTurn, isPingerTurn).start();
	
		isPingerTurn.release();
	}

}
