package pcd.lab03.sync;

/**
 * nel package synchronizer sono presenti tutti i concetti utili per fare
 * la sincronizzazione in java
 *
 * implementazioni:
 * LOCK -> equivalente delle mutex [mi permettono di fare mutua esclusione] (sono rientranti)
 * SEMAPHORE -> semafore in java (metodi presenti aquire e release) sono stati creati più verso i permessi
 * si crea specificando il numero iniziale di permessi (NON USARE K NEGATIVI! SERVE PER I PERMESSI)
 * possono essere usati anche come event semaphore. Si può anche specificare la fairness
 *
 */
public class Counter {

	private int cont;
	
	public Counter(int base){
		this.cont = base;
	}
	
	public void inc(){
		cont++;
	}
		
	
	public int getValue(){
		return cont;
	}
	
}
